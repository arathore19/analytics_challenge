import duckdb
import os
from insert_data import generate_base_tables
from views.geospatial_user_visit_view import create_geospatial_user_visit_view
from views.geospatial_user_bookmark_view import create_geospatial_user_bookmark_view
from models.user_engagement_by_hood_and_quarter import create_user_engagement_by_hood_quarter
from models.user_retention_churn import create_user_retention_churn
from models.bookmark_count import create_bookmark_count

def generate_views():
    create_geospatial_user_visit_view()
    create_geospatial_user_bookmark_view()


def generate_models():
    model_csv = ['user_engagement_by_hood_quarter_model', 'user_retention_churn_model', 'bookmark_count_model']

    create_user_engagement_by_hood_quarter()
    create_user_retention_churn()
    create_bookmark_count()

    output_dir = 'model_files'
    os.makedirs('model_files', exist_ok=True)

    for table in model_csv:
        output_file_path = os.path.join(output_dir, f'{table}.csv')
        copy_query = f"COPY {table} TO '{output_file_path}' (FORMAT CSV, HEADER)"
        duckdb.sql(copy_query)


if __name__ == "__main__":
    generate_base_tables()
    generate_views()
    generate_models()
    
