import duckdb

def create_bookmark_count():
    create_bookmark_count_query = """
        CREATE TABLE bookmark_count_model AS
        SELECT DISTINCT
            HOOD_NAME,
            COUNT(DISTINCT BOOKMARK_ID) AS NUM_BOOKMARKS
        FROM 
            geospatial_user_bookmark_view
        GROUP BY 
            HOOD_NAME
        ORDER BY 
            NUM_BOOKMARKS DESC;
    """

    duckdb.sql(create_bookmark_count_query)