import duckdb

def create_user_engagement_by_hood_quarter():
    create_user_engagement_by_hood_quarter_query = """
    CREATE TABLE user_engagement_by_hood_quarter_model AS
        SELECT DISTINCT
            HOOD_NAME,
            VISIT_QUARTER,
            COUNT(VISIT_ID) AS VISIT_COUNT,
            COUNT(DISTINCT USER_ID) AS UNIQUE_USERS
        FROM
            geospatial_user_visit_view
        GROUP BY
            HOOD_NAME, VISIT_QUARTER
        ORDER BY 
            HOOD_NAME, VISIT_QUARTER;
    """

    duckdb.sql(create_user_engagement_by_hood_quarter_query)