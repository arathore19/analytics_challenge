import duckdb

def create_user_retention_churn():
    create_user_retention_churn_query = """
    CREATE TABLE user_retention_churn_model AS
        WITH user_retention_churn_temp AS (
            SELECT
                USER_ID,
                VISIT_QUARTER,
                LAG(VISIT_QUARTER) OVER (PARTITION BY USER_ID ORDER BY VISIT_QUARTER) AS PREV_QUARTER
            FROM
                geospatial_user_visit_view
        )
        SELECT
            PREV_QUARTER,
            VISIT_QUARTER,
            COUNT(DISTINCT USER_ID) AS RETURNING_USERS
        FROM
            user_retention_churn_temp
        WHERE
            PREV_QUARTER IS NOT NULL
        GROUP BY
            PREV_QUARTER, VISIT_QUARTER;
    """

    duckdb.sql(create_user_retention_churn_query)