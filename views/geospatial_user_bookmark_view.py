import duckdb

def create_geospatial_user_bookmark_view():
    create_geospatial_user_bookmark_view_query = """
    CREATE VIEW geospatial_user_bookmark_view AS
        SELECT DISTINCT
            bk.ID AS BOOKMARK_ID,
            uc.ID AS USER_ID,
            uc.NAME || ' ' || uc.LAST_NAME AS fullname,
            hc.HOOD_ID,
            hc.HOOD_NAME,
            hc.CITY,
            bk.CREATED_AT AS BOOKMARK_CREATED_AT,
            EXTRACT(QUARTER FROM CAST(bk.CREATED_AT AS DATE)) AS CREATED_AT_QUARTER,
            EXTRACT(YEAR FROM CAST(uc.BIRTHDATE AS DATE)) AS BIRTH_YEAR,
            CAST(FLOOR(EXTRACT(YEAR FROM CAST(uc.BIRTHDATE AS DATE)) / 10) * 10 AS INT) AS BIRTH_DECADE
        FROM
            bookmark_data bk
        LEFT JOIN
            user_creation_data uc ON bk.USER_ID = uc.ID
        LEFT JOIN
            hood_creation_data hc ON bk.HOOD_ID = hc.HOOD_ID;
        """


    duckdb.sql(create_geospatial_user_bookmark_view_query)