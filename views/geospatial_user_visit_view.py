import duckdb

def create_geospatial_user_visit_view():
    create_geospatial_user_visit_view_query = """
    CREATE VIEW geospatial_user_visit_view AS
        SELECT DISTINCT
            uv.ID AS VISIT_ID,
            uc.ID AS USER_ID,
            uc.NAME || ' ' || uc.LAST_NAME AS fullname,
            hc.HOOD_ID,
            hc.HOOD_NAME,
            hc.CITY,
            uv.VISIT_DATE AS USER_VISIT_DATE,
            EXTRACT(QUARTER FROM CAST(uv.VISIT_DATE AS DATE)) AS VISIT_QUARTER,
            EXTRACT(YEAR FROM CAST(uc.BIRTHDATE AS DATE)) AS BIRTH_YEAR,
            CAST(FLOOR(EXTRACT(YEAR FROM CAST(uc.BIRTHDATE AS DATE)) / 10) * 10 AS INT) AS BIRTH_DECADE
        FROM
            user_visit_data uv
        LEFT JOIN
            user_creation_data uc ON uv.USER_ID = uc.ID
        LEFT JOIN
            hood_creation_data hc ON uv.HOOD_ID = hc.HOOD_ID;
        """

    duckdb.sql(create_geospatial_user_visit_view_query)
