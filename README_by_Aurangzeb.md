### 

Executing the main file will generate a rudimentary pipeline in a systematic way.

Firstly, base the tables will be formed. Data will be extracted from the event_example.json file and undergo basic transformation
such as data type definition and consistency of the data types. The models are output as csv files (in base_output_files directory)
for further inspection.

For the second layer, business logic is applied through JOINING to create views. These views can be used to perform further sql 
queries on that can produce more insightful results.

The third layer uses these views to produce models that are actionable for decision-making. Due to the limited nature of the provided 
data I have a produced a few logical extension of what could be designed in order to extract value. The models are then stored in 
directory model_files as csv.

Please note that I did not create any foreign key constraints as there are some missing values (probably due to the data being a 
subset).
