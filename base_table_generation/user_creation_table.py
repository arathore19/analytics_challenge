
# Create a table for user_creation
create_user_creation_table = """
CREATE TABLE user_creation_data (
    EVENT_TYPE STRING,
    ID UUID PRIMARY KEY,
    NAME STRING,
    LAST_NAME STRING,
    BIRTHDATE STRING
);
"""

# Insert data into user_creation table
insert_user_creation = """
INSERT INTO user_creation_data
SELECT EVENT_TYPE, ID, NAME, LAST_NAME, BIRTHDATE
FROM nebenan_events
WHERE EVENT_TYPE = 'user_creation';
"""