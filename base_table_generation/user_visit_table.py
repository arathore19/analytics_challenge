
# Create a table for user_visit
create_user_visit_table = """
CREATE TABLE user_visit_data (
    EVENT_TYPE STRING,
    HOOD_ID UUID,
    ID UUID PRIMARY KEY,
    USER_ID UUID,
    VISIT_DATE STRING
);
"""

# Insert data into user_visit table
insert_user_visit = """
INSERT INTO user_visit_data
SELECT EVENT_TYPE, HOOD_ID, ID, USER_ID, VISIT_DATE
FROM nebenan_events
WHERE EVENT_TYPE = 'user_visit';
"""