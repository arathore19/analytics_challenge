
# Create a table for hood_creation
create_hood_creation_table = """
CREATE TABLE hood_creation_data (
    CITY STRING,
    CREATED_AT TIMESTAMP,
    EVENT_TYPE STRING,
    HOOD_ID UUID PRIMARY KEY,
    HOOD_NAME STRING,
    STATE STRING
);
"""

# Insert data into hood_creation table
insert_hood_creation = """
INSERT INTO hood_creation_data
SELECT CITY, CREATED_AT, EVENT_TYPE, HOOD_ID, HOOD_NAME, STATE
FROM nebenan_events
WHERE EVENT_TYPE = 'hood_creation';
"""