
# Create a table for bookmark
create_bookmark_table = """
CREATE TABLE bookmark_data (
    CREATED_AT TIMESTAMP,
    EVENT_TYPE STRING,
    HOOD_ID UUID,
    HOOD_MESSAGE_ID STRING,
    ID STRING PRIMARY KEY,
    USER_ID STRING
);
"""

# Insert data into bookmark table
insert_bookmark = """
INSERT INTO bookmark_data
SELECT CREATED_AT, EVENT_TYPE, HOOD_ID, HOOD_MESSAGE_ID, ID, USER_ID
FROM nebenan_events
WHERE EVENT_TYPE = 'bookmark';
"""