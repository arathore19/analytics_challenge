## Analytics Engineer Coding Assignment

### Introduction

Thank you for your interest in the Analytics Engineer position at Nebenan.

As part of our recruiting process, we kindly request all candidates to submit a solution to a data challenge. We appreciate your efforts in completing this task within a time limit of 3 hours. Our main focus is to understand your approach to data modeling rather than expecting a fully developed solution.

### The task

Nebenan is the largest neighbors' platform in Germany, generating a significant volume of events related to user interactions.

The event_example.json contains a small subset of the data we handle. Your task is to reorganize this data into various models, enabling stakeholders such as data analysts and product managers to utilize it effectively for further analysis and decision-making.

### Setup instructions

In this repo you can find an `insert_data.py` and a `requirements.txt` file. To run the `insert_data.py` you will need to install the packages from `requirements.txt`. We recommend that you use a virtual environment for this. In case you need more packages for creating the models, update the `requirements.txt` correspondingly.

The `insert_data.py` inserts the data into a [`duckdb`](https://duckdb.org) instance. Feel free to modify and rename this file adding further transformation steps.

In general, you are free to use whatever you want to produce the data models. However, remember to add some instructions on how to run them in case is needed.

### Deliverables

Produce a set of database objects (e.g. tables, views) based on the data provided.
