# We will generate base relational models

import duckdb
import os
from base_table_generation.bookmark_table import create_bookmark_table, insert_bookmark
from base_table_generation.hood_creation_table import create_hood_creation_table, insert_hood_creation
from base_table_generation.user_visit_table import create_user_visit_table, insert_user_visit
from base_table_generation.user_creation_table import create_user_creation_table, insert_user_creation


def generate_base_tables():
    insert_data = duckdb.sql("create table nebenan_events as select * from 'events_example.json';")

    create_table_queries = [create_bookmark_table, create_hood_creation_table, create_user_visit_table, create_user_creation_table]
    insert_table_queries = [insert_bookmark, insert_hood_creation, insert_user_visit, insert_user_creation]
    csv_tables = ['user_creation_data', 'bookmark_data', 'hood_creation_data', 'user_visit_data']

    for my_query in create_table_queries:
        duckdb.sql(my_query)

    for my_query in insert_table_queries:
        duckdb.sql(my_query)

    output_dir = 'base_output_files'
    os.makedirs('base_output_files', exist_ok=True)

    for table in csv_tables:
        output_file_path = os.path.join(output_dir, f'{table}.csv')
        copy_query = f"COPY {table} TO '{output_file_path}' (FORMAT CSV, HEADER)"
        duckdb.sql(copy_query)

